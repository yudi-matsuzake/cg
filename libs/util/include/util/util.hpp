#pragma once

#include <string_view>
#include <algorithm>
#include <utility>

namespace util{

template<class T>
constexpr auto make_cast() noexcept
{
	return [](auto&& x)
	{
		using from_type = decltype(x);
		return static_cast<T>(std::forward<from_type>(x));
	};
}

template<class T>
constexpr auto make_interpolation(T min_a, T max_a, T min_b, T max_b)
{
	return [min_a, max_a, min_b, max_b](auto x)
	{
		return min_b + (x - min_a) / (max_a - min_a) * (max_b - min_b);
	};
}

template<class T>
constexpr auto make_clamp(T lo, T hi)
{
	return [lo, hi](auto x)
	{
		return std::clamp(x, lo, hi);
	};
}

constexpr auto begins_with(std::string_view a, std::string_view b)
{
	if(b.size() > a.size())
		return false;
	for(auto i=0UL; i<b.size(); ++i)
		if(b[i] != a[i])
			return false;
	return true;
}

} // end of namespace util
