#pragma once

#include <algorithm>
#include <string_view>

#include "util/util.hpp"
#include "arithmetic-container.hpp"

namespace cg::colors{

enum class model : int8_t{
	NONE = -1,
	RGB,
	XYZ,
	CMY,
	HSV,
	CIELAB,
	COUNT
};

using model_name_array = std::array<
	std::string_view,
	static_cast<uint64_t>(model::COUNT)
>;

constexpr model_name_array model_names{
	"rgb",
	"xyz",
	"cmy",
	"hsv",
	"cielab"
};

constexpr auto to_string(model model)
{
	return model_names[static_cast<uint32_t>(model)];
}

constexpr auto to_model(std::string_view str)
{

	auto count = static_cast<int8_t>(model::COUNT);
	auto to_model = util::make_cast<enum model>();

	for(int8_t i=0; i<count; ++i)
		if(auto model = to_model(i); str == to_string(model))
			return model;

	return model::NONE;
}

constexpr auto toui = util::make_cast<uint64_t>();

template<class T>
constexpr std::string_view name_of;

template<>
constexpr std::string_view name_of<struct rgb> = model_names[toui(model::RGB)];

template<>
constexpr std::string_view name_of<struct xyz> = model_names[toui(model::XYZ)];

template<>
constexpr std::string_view name_of<struct hsv> = model_names[toui(model::HSV)];

template<>
constexpr std::string_view name_of<struct cielab> = model_names[toui(model::CIELAB)];

template<>
constexpr std::string_view name_of<struct cmy> = model_names[toui(model::CMY)];

struct rgb : public arithmetic_container<uint8_t, 3>{
private:
	using arithmetic_container<uint8_t, 3>::x;
	using arithmetic_container<uint8_t, 3>::y;
	using arithmetic_container<uint8_t, 3>::z;
	using arithmetic_container<uint8_t, 3>::t;
	using arithmetic_container<uint8_t, 3>::s;

public:
	rgb() = default;

	// for initializations like rgb{ L, a, b }
	template<class FirstType, class SecondType, class ... Args>
	constexpr rgb(FirstType&& f, SecondType&& s, Args&& ... args) :
		arithmetic_container<uint8_t, 3>::arithmetic_container(
			std::forward<FirstType>(f),
			std::forward<SecondType>(s),
			std::forward<Args>(args) ...
		)
	{}

	constexpr rgb(rgb&& c)
		: arithmetic_container<uint8_t, 3>(std::forward<rgb>(c))
	{}

	rgb(rgb const& c)
		: arithmetic_container<uint8_t, 3>(c)
	{}


	auto linearize() const
	{
		arithmetic_container<scalar, 3> l;

		using namespace util;
		rgs::copy(*this
			| vws::transform(make_cast<scalar>())
			| vws::transform(make_interpolation(0., 255., 0., 1.)),
			std::begin(l)
		);

		return l;
	}

	static auto from_linearized(arithmetic_container<scalar, 3> l)
	{
		using namespace util;

		rgb from;
		auto tci = to_closest_integer<uint8_t>;
		rgs::copy(l
			| vws::transform(make_interpolation(0., 1., 0., 255.))
			| vws::transform(tci),
			std::begin(from)
		);
		return from;
	}
};

struct xyz : public arithmetic_container<scalar, 3>{
private:
	using arithmetic_container<scalar, 3>::r;
	using arithmetic_container<scalar, 3>::g;
	using arithmetic_container<scalar, 3>::b;
	using arithmetic_container<scalar, 3>::s;
	using arithmetic_container<scalar, 3>::t;


	constexpr static glm::mat3x3 from_rgb_to_xyz_m{
		0.4360747, 0.2225045, 0.0139322,
		0.3850649, 0.7168786, 0.0971045,
		0.1430804, 0.0606169, 0.7141733
	};

	constexpr static glm::mat3x3 from_xyz_to_rgb_m {
		 3.1338561, -0.9787684, 0.0719453,
		-1.6168667, 1.9161415, -0.2289914,
		-0.4906146, 0.0334540, 1.4052427
	};

	static rgb to_rgb(xyz const& c)
	{
		static constexpr auto gamma_correction = [](auto u)
		{
			if(u <= 0.0031308)
				return u*323./ 25.;
			return (211. * std::pow(u, 5./12.) - 11) / 200.;
		};

		arithmetic_container<scalar, 3> v = from_xyz_to_rgb_m * c;

		auto tci = to_closest_integer<uint8_t>;

		using namespace util;
		auto conversion = v
			| vws::transform(gamma_correction)
			| vws::transform(make_interpolation(0., 1., 0., 255.))
			| vws::transform(tci);
		
		rgb converted;
		rgs::copy(conversion, std::begin(converted));
		return converted;
	}

	static xyz to_xyz(rgb const& c)
	{

		static constexpr auto gamma_correction = [](auto v)
		{
			if(v <= 0.04045)
				return v / 12.92;
			return std::pow((v + 0.055) / 1.055, 2.4);
		};

		using namespace util;

		arithmetic_container<scalar, 3> v;
		rgs::copy(c
			| vws::transform(make_cast<scalar>())
			| vws::transform(make_interpolation(0., 255., 0., 1.))
			| vws::transform(gamma_correction),
			std::begin(v)
		);

		xyz r;
		v = from_rgb_to_xyz_m * v;
		rgs::copy(v, std::begin(r));
		return r;
	}

public:
	xyz() = default;

	// for initializations like xyz{ L, a, b }
	template<class FirstType, class SecondType, class ... Args>
	constexpr xyz(FirstType&& f, SecondType&& s, Args&& ... args) :
		arithmetic_container<scalar, 3>::arithmetic_container(
			std::forward<FirstType>(f),
			std::forward<SecondType>(s),
			std::forward<Args>(args) ...
		)
	{}

	constexpr xyz(xyz&& c)
		: arithmetic_container<scalar, 3>(std::forward<xyz>(c))
	{}

	xyz(xyz const& c)
		: arithmetic_container<scalar, 3>(c)
	{}

	xyz(rgb const& c)
		: xyz(to_xyz(c))
	{}

	operator rgb() const
	{
		return to_rgb(*this);
	}
};

struct hsv : public arithmetic_container<scalar, 3>{
private:

	using arithmetic_container<scalar, 3>::x;
	using arithmetic_container<scalar, 3>::y;
	using arithmetic_container<scalar, 3>::z;
	using arithmetic_container<scalar, 3>::r;
	using arithmetic_container<scalar, 3>::g;
	using arithmetic_container<scalar, 3>::b;
	using arithmetic_container<scalar, 3>::t;

	static rgb to_rgb(hsv const& c)
	{
		auto chroma = c.v * c.s;
		auto h_prime = c.h / 60.;

		auto x = chroma * (1 - std::abs(std::fmod(h_prime, 2.) - 1));

		using s = arithmetic_container<scalar, 3>;

		auto r1 = s{};

		if(c.h == 0.0)
			r1 = s{0, 0, 0};
		else if(h_prime > 0 && h_prime <= 1.)
			r1 = s{ chroma, x, 0.0 };
		else if(h_prime > 1 && h_prime <= 2.)
			r1 = s{ x, chroma, 0.0 };
		else if(h_prime > 2 && h_prime <= 3.)
			r1 = s{ 0.0, chroma, x };
		else if(h_prime > 3 && h_prime <= 4.)
			r1 = s{ 0.0, x, chroma };
		else if(h_prime > 4 && h_prime <= 5.)
			r1 = s{ x, 0.0, chroma };
		else if(h_prime > 5 && h_prime <= 6.)
			r1 = s{ chroma, 0.0, x };

		auto m = c.v - chroma;
		r1 += m;

		return rgb::from_linearized(r1);
	}

	// h1
	static auto compute_lhs_vars(rgb const& rgb)
	{
		auto c = rgb.linearize();

		auto [emin, emax] = rgs::minmax_element(c);
		scalar min = *emin;
		scalar max = *emax;
		scalar chroma = max - min;

		scalar h = [&]
		{
			if(chroma == 0)
				return 0.0;

			scalar tmp = 0.0;
			scalar add = 0.0;

			if(c.r == max){
				tmp = c.g - c.b;
				if(c.g < c.b)
					add = 6.;
			}else if(c.g == max){
				tmp = c.b - c.r;
				add = 2.;
			}else{
				tmp = c.r - c.g;
				add = 4.;
			}

			return 60. * (tmp / chroma + add);
		}();

		return std::tuple{ min, max, chroma, h };
	}

	// h2
	/* static auto compute_lhs_vars(rgb const& rgb) */
	/* { */
	/* 	static auto const half_sqrt3 = std::sqrt(3) / 2.; */

	/* 	auto c = rgb.linearize(); */

	/* 	scalar min = std::min({ c.r, c.g, c.b }); */
	/* 	scalar max = std::max({ c.r, c.g, c.b }); */

	/* 	auto alpha	= 1./2. * (2 * c.r - c.g - c.b); */
	/* 	auto beta	= half_sqrt3 * (c.g - c.b); */

	/* 	scalar h = std::atan2(beta, alpha); */
	/* 	scalar chroma = std::sqrt(alpha*alpha + beta*beta); */
	/* 	return std::tuple{ min, max, chroma, h * 180./M_PI }; */
	/* } */

	static hsv to_hsv(rgb const& c)
	{
		auto [min, max, chroma, h] = compute_lhs_vars(c);

		auto v = max;
		auto s = v == 0 ? 0 : chroma / v;

		return hsv{ h, s, v };
	}

public:
	/* using arithmetic_container<scalar, 3>::arithmetic_container; */

	hsv() : h(x), s(y), v(z)
	{}

	// for initializations like hsv{ h, s, v }
	template<class FirstType, class SecondType, class ... Args>
	hsv(FirstType&& f, SecondType&& s, Args&& ... args) :
		arithmetic_container<scalar, 3>::arithmetic_container(
			std::forward<FirstType>(f),
			std::forward<SecondType>(s),
			std::forward<Args>(args) ...
		),
		h(x), s(y), v(z)
	{}

	hsv(hsv&& c)
		: arithmetic_container<scalar, 3>(std::forward<hsv>(c)),
		  h(x), s(y), v(z)
	{}

	hsv(hsv const& c)
		: arithmetic_container<scalar, 3>(c),
		  h(x), s(y), v(z)
	{}

	hsv(rgb const& c) : hsv(to_hsv(c))
	{}

	operator rgb() const
	{
		return to_rgb(*this);
	}

	scalar& h;
	scalar& s;
	scalar& v;
};

struct cmy : public arithmetic_container<scalar, 3>{
private:
	using arithmetic_container<scalar, 3>::x;
	using arithmetic_container<scalar, 3>::y;
	using arithmetic_container<scalar, 3>::z;
	using arithmetic_container<scalar, 3>::r;
	using arithmetic_container<scalar, 3>::g;
	using arithmetic_container<scalar, 3>::b;
	using arithmetic_container<scalar, 3>::s;
	using arithmetic_container<scalar, 3>::t;

	static cmy from_rgb(rgb const& c)
	{
		using namespace util;
		auto l = c.linearize();
		auto neg = [](auto x){ return 1. - x; };
		cmy r;
		rgs::copy(l
			| vws::transform(neg)
			| vws::transform(make_interpolation(0., 1., 0., 100.)),
			std::begin(r)
		);

		return r;
	}

	static rgb to_rgb(cmy const& c)
	{
		using namespace util;
		auto neg = [](auto x){ return 100. - x; };

		auto tci = to_closest_integer<uint8_t>;
		rgb r;
		rgs::copy(c
			| vws::transform(neg)
			| vws::transform(make_interpolation(0., 100., 0., 255.))
			| vws::transform(tci),
			std::begin(r)
		);

		return r;
	}

public:

	cmy() : C(x), M(y), Y(z)
	{}

	// for initializations like cmy{ C, M, Y }
	template<class FirstType, class SecondType, class ... Args>
	cmy(FirstType&& f, SecondType&& s, Args&& ... args) :
		arithmetic_container<scalar, 3>::arithmetic_container(
			std::forward<FirstType>(f),
			std::forward<SecondType>(s),
			std::forward<Args>(args) ...
		),
		C(x), M(y), Y(z)
	{}

	cmy(cmy&& c)
		: arithmetic_container<scalar, 3>(std::forward<cmy>(c)),
		  C(x), M(y), Y(z)
	{}

	cmy(cmy const& c)
		: arithmetic_container<scalar, 3>(c),
		  C(x), M(y), Y(z)
	{}

	cmy(rgb const& c)
		: cmy(from_rgb(c))
	{}

	operator rgb() const
	{
		return to_rgb(*this);
	}

	scalar& C;
	scalar& M;
	scalar& Y;
};

struct cielab : public arithmetic_container<scalar, 3>{
public:
	static constexpr xyz illuminant{ .964212, 1.00, .825188 };
	static constexpr auto delta = 6. / 29.;

	static cielab to_cielab(xyz const& c)
	{

		auto const f = [](auto t)
		{
			static auto const delta_2 = std::pow(delta, 2.);
			static auto const delta_3 = std::pow(delta, 3.);
			static auto const four_29 = 4./29.;

			if(t > delta_3)
				return std::pow(t, 1./3.);
			return (t / 3.*delta_2) + (four_29);
		};

		cielab r;

		auto const fx = f(c.x / illuminant.x);
		auto const fy = f(c.y / illuminant.y);
		auto const fz = f(c.z / illuminant.z);

		r.L = 116. * fy - 16.;
		r.a = 500. * (fx - fy);
		r.b = 200. * (fy - fz);

		return r;
	}

	static xyz to_xyz(cielab const& c)
	{
		auto const f_inv = [](auto t)
		{
			if(t > delta)
				return std::pow(t, 3.);
			return 3. * std::pow(delta, 2.) * (t - 4./29.);
		};

		xyz r;

		r.x = illuminant.x * f_inv((c.L + 16.) / 116. + c.a / 500.);
		r.y = illuminant.y * f_inv((c.L + 16.) / 116.);
		r.z = illuminant.z * f_inv((c.L + 16.) / 116. - c.b / 200.);

		return r;
	}

public:

	cielab() : L(x), a(y), b(z)
	{}

	// for initializations like cielab{ L, a, b }
	template<class FirstType, class SecondType, class ... Args>
	cielab(FirstType&& f, SecondType&& s, Args&& ... args) :
		arithmetic_container<scalar, 3>::arithmetic_container(
			std::forward<FirstType>(f),
			std::forward<SecondType>(s),
			std::forward<Args>(args) ...
		),
		L(x), a(y), b(z)
	{}

	cielab(cielab&& c)
		: arithmetic_container<scalar, 3>(std::forward<cielab>(c)),
		  L(x), a(y), b(z)
	{}

	cielab(cielab const& c)
		: arithmetic_container<scalar, 3>(c),
		  L(x), a(y), b(z)
	{}

	cielab(xyz const& c)
		: cielab(to_cielab(c))
	{}

	cielab(rgb const& c)
		: cielab(to_cielab(c))
	{}

	operator xyz() const
	{
		return to_xyz(*this);
	}

	operator rgb() const
	{
		return to_xyz(*this);
	}

	scalar& L;
	scalar& a;
	scalar& b;
};

template<class To, class From>
To convert_to(From const& from)
{
	if constexpr(std::is_constructible_v<To, From const&>){
		return To(from);
	}else{
		static_assert(
			std::is_constructible_v<To, cg::colors::rgb const&>,
			"The color must be convertible to RGB"
			" to perform this conversion!"
		);

		return cg::colors::rgb(from);
	}
}

} // end of namespace cg
