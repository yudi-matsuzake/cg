#pragma once

#include <utility>

#include "glm/vec2.hpp"

namespace cg{

class segment2d : public std::pair<glm::vec2, glm::vec2>{
public:
	using std::pair<glm::vec2, glm::vec2>::pair;
};

} // end of namespace cg
