#pragma once

#include <range/v3/view/cycle.hpp>
#include <range/v3/view/drop_exactly.hpp>
#include <vector>

#include "cg/misc.hpp"
#include "cg/geometric-miscellaneous.hpp"
#include "cg/segment2d.hpp"
#include "util/util.hpp"

namespace cg{

class polygon2d : public std::vector<glm::vec2>{
public:
	using std::vector<glm::vec2>::vector;
};

polygon2d operator*(polygon2d const& p, cg::scalar s);
polygon2d operator*(cg::scalar s, polygon2d const& p);
polygon2d operator+(polygon2d const& p, glm::vec2 const& v);
polygon2d operator-(polygon2d const& p, glm::vec2 const& v);

template<class PolygonType>
auto segments_view(PolygonType&& poly)
{
	auto to_segment = [](auto&& seg)
	{
		auto&& [p, q] = seg;
		return segment2d{p, q};
	};

	auto two_by_two = vws::zip(
		poly | vws::cycle | vws::take(poly.size()),
		poly | vws::cycle | vws::drop_exactly(1)
	);

	return  two_by_two | vws::transform(to_segment);
}

} // end of namespace cg
