#pragma once
#include <iostream>

#include "cg/misc.hpp"
#include "util/util.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"

namespace cg{

template<class T, uint64_t N>
struct arithmetic_container : public glm::vec<N, T>{
	using glm::vec<N, T>::vec;

	constexpr auto begin()
	{
		return &((*this)[0]);
	}

	constexpr auto end()
	{
		return begin() + N;
	}

	constexpr auto begin() const
	{
		return &((*this)[0]);
	}

	constexpr auto end() const
	{
		return begin() + N;
	}

};

template<class T, uint64_t N>
std::ostream& operator<<(
	std::ostream& out,
	arithmetic_container<T, N> const& con)
{
	std::for_each(
		std::begin(con), std::end(con) - 1,
		[&](auto&& x)
		{
			out << x << ' ';
		}
	);

	auto last = *(std::end(con) - 1);
	out << last;

	return out;
}

template<class To, class From, uint64_t N>
auto arithmetic_container_cast(arithmetic_container<From, N> const& ac)
{
	arithmetic_container<To, N> r;

	auto casted = ac | vws::transform(util::make_cast<To>());

	rgs::copy(casted, std::begin(r));

	return r;
}

} // end of namespace cg
