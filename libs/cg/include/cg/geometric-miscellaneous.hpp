#pragma once

#include <optional>
#include <variant>
#include <cstdint>

#include "cg/misc.hpp"
#include "cg/segment2d.hpp"

namespace cg{

enum class orientation : int8_t {
	COLLINEAR,
	LEFT,
	RIGHT
};

/**
  * computes the orientation between three points
  *
  * this functions can return either orientation::LEFT,
  * orientation::RIGHT, or orientation::COLLINEAR. These
  * status are base on whether `c` is left, right or collinear
  * with the segment composed by `a` and `b`
  *
  */
orientation compute_orientation(
	glm::vec2 const& a,
	glm::vec2 const& b,
	glm::vec2 const& c
);

bool is_colinear(glm::vec2 const& a, glm::vec2 const& b, glm::vec2 const& c);
bool is_left(glm::vec2 const& a, glm::vec2 const& b, glm::vec2 const& c);
bool is_right(glm::vec2 const& a, glm::vec2 const& b, glm::vec2 const& c);

bool is_colinear(segment2d const& s, glm::vec2 const& p);
bool is_left(segment2d const& s, glm::vec2 const& p);
bool is_right(segment2d const& s, glm::vec2 const& p);

/**
  * checks whether `p` lies on the segment s
  */
bool is_between(segment2d const& s, glm::vec2 const& p);
bool is_between(glm::vec2 const& a, glm::vec2 const& b, glm::vec2 const& c);


/**
  * returns true if segments `a` and `b` intersects
  */
bool intersects(cg::segment2d const& a, cg::segment2d const& b);

using intersection_between_seg2d = std::optional<
	std::variant<glm::vec2, cg::segment2d>
>;

intersection_between_seg2d compute_intersection(
	cg::segment2d const& a,
	cg::segment2d const& b
);

} // end of namespace cg
