#include "cg/misc.hpp"
#include "cg/math.hpp"
#include <range/v3/view/enumerate.hpp>

namespace cg{

template<uint64_t Dimension>
class bezier_curve : public std::vector<glm::vec<Dimension, float>>{
public:
	using vec_type = glm::vec<Dimension, cg::scalar>;

	vec_type operator()(cg::scalar t)
	{
		vec_type v(0.f);

		auto n = this->size() - 1;

		if(n <= 0) return v;

		for(auto&& [ k, p ] : vws::enumerate(*this)){
			float bez = binomial_coefficient(n, k)
				* std::pow(t, k)
				* std::pow(cg::scalar{1} - t, n - k);
			
			v += p*bez;
		}

		return v;
	}
};

} // end of namespace cg
