#pragma once

#include "glm/vec2.hpp"
#include "glm/ext/scalar_constants.hpp"
#include "glm/ext/matrix_transform.hpp"
#include "glm/gtx/rotate_vector.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/ext/matrix_clip_space.hpp"
#include "glm/gtx/transform.hpp"

#include "range/v3/all.hpp"
#include "fmt/format.h"
#include "fmt/ostream.h"

namespace cg{

namespace rgs = ranges;
namespace vws = ranges::views;
namespace acs = ranges::actions;

using scalar = double;

template<class IntegerType>
constexpr auto to_closest_integer(scalar t)
{
	auto f = t < scalar{0} ? scalar{-0.5} : scalar{+0.5};
	return static_cast<IntegerType>(t + f);
}

} // end of namespace cg

