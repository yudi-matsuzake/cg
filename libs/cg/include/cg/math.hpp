#include "cg/misc.hpp"

namespace cg{

constexpr auto factorial(int64_t n)
{
	auto fac = 1LL;
	for(auto i = 2; i <= n; ++i)
		fac *= i;
	return fac;
}

constexpr auto binomial_coefficient(int64_t n, int64_t k)
{
	return factorial(n) / (factorial(k) * factorial(n - k));
}

} // end of namespace cg
