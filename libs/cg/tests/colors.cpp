#include <numeric>
#include <type_traits>

#include "fmt/format.h"
#include "fmt/ostream.h"
#include "catch2/catch.hpp"
#include "cg/colors.hpp"
#include "cg/arithmetic-container.hpp"

namespace rgs = ranges;
namespace vws = rgs::views;
namespace acs = rgs::actions;

template<class From, class To>
auto check_conversion_one_way(From const& from, To const& ideal)
{
	constexpr auto eps = .5;
	constexpr auto is_almost_equal = [eps](auto x, auto y)
	{
		auto ts = util::make_cast<cg::scalar>();
		return std::abs(ts(x) - ts(y)) < eps;
	};

	auto converted = cg::colors::convert_to<To>(from);

	UNSCOPED_INFO(fmt::format(
		"converted: {}",
		cg::arithmetic_container_cast<cg::scalar>(converted)
	));

	for(auto&& [ co, id ] : vws::zip(converted, ideal)){
		if(!is_almost_equal(co, id))
			return false;
	}

	return true;
}

template<class A, class B>
auto check_both_ways(A const& a, B const& b)
{
	using namespace cg::colors;

	UNSCOPED_INFO(fmt::format("{} -> {}", name_of<A>, name_of<B>));
	if(!check_conversion_one_way(a, b))
		return false;

	UNSCOPED_INFO(fmt::format("{} -> {}", name_of<B>, name_of<A>));
	return check_conversion_one_way(b, a);
}

template<class T>
bool check_conversions0(T&&){ return true; }

template<class First, class Second, class ... Args>
bool check_conversions0(First&& f, Second&& s, Args&&... args)
{
	if(!check_both_ways(std::forward<First>(f), std::forward<Second>(s)))
		return false;
	return check_conversions0(std::forward<First>(f), std::forward<Args>(args) ...);
}

bool check_conversions(){ return true; }

template<class First, class ... Args>
bool check_conversions(First&& f, Args&&... args)
{
	if(!check_conversions0(std::forward<First>(f), std::forward<Args>(args)...))
		return false;

	return check_conversions(std::forward<Args>(args)...);
}

TEST_CASE("Colors simple tests", "[colors]")
{
	using namespace cg::colors;

	// test 1
	REQUIRE(check_conversions(
		rgb{255,	255,	255},
		xyz {0.964220,	1.00,	0.825210},
		hsv {0.0,	0.0,	1.},
		cmy{ 0.0, 0.0, 0.0 },
		cielab{ 100.0, 0.0, 0.0 }
	));

	// test 2
	REQUIRE(check_conversions(
		rgb{100,	255,		0},
		xyz{0.440637,	0.745234,	0.098880},
		hsv{96.5,	1.,		1. },
		cmy{ 60.78,	0.0,		100.0 }
	));

	// test 3
	REQUIRE(check_conversions(
		rgb{50,	50,		255},
		xyz{0.169271,	0.090580,	0.717715},
		hsv{ 240,	0.804,		1.0 },
		cmy{ 80.39,	80.39,		0.0 }
	));

	// test 4
	REQUIRE(check_conversions(
		rgb{0,		0,	0},
		xyz{0.,		0., 	0.},
		hsv{ 0.0,	0.0,	0.0 },
		cmy{ 100.0,	100.0,	100.0 }
	));

	// test 5
	REQUIRE(check_conversions(
		rgb{255,	0,	1},
		xyz{.436118,	.222523,.014149},
		hsv{ 359.76,	1.,	1. },
		cmy{ 0., 100.0,	99.61 },
		cielab{ 54.2937, 80.8169, 69.6237 }
	));

	// test 6
	REQUIRE(check_conversions(
		rgb{196,	9,	29},
		xyz{.243528,	.125528,.016731},
		hsv{ 353.58,	.9541,	.7686 },
		cmy{ 23.1372, 96.4705, 88.6274 },
		cielab{ 42.0816, 65.7022, 45.6051 }
	));
}
