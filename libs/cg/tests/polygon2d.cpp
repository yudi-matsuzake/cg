#include "catch2/catch.hpp"

#include "cg/polygon2d.hpp"
#include "cg/segment2d.hpp"

namespace rgs = ranges;
namespace vws = ranges::views;
namespace acs = ranges::actions;

TEST_CASE("segments_view", "[polygon2d]")
{
	cg::polygon2d poly{ { 0., 0. }, { 1., 0. }, { 1., 1. }, { 0., 1. } };

	REQUIRE(rgs::equal(
		cg::segments_view(poly),
		std::array{
			cg::segment2d{ { 0., 0. }, { 1., 0. } },
			cg::segment2d{ { 1., 0. }, { 1., 1. } },
			cg::segment2d{ { 1., 1. }, { 0., 1. } },
			cg::segment2d{ { 0., 1. }, { 0., 0. } }
		}
	));
}
