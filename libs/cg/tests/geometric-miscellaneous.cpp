#include "catch2/catch.hpp"

#include <functional>
#include <range/v3/action/action.hpp>
#include <range/v3/range/concepts.hpp>
#include <range/v3/view/subrange.hpp>
#include <range/v3/view/view.hpp>
#include <stdexcept>
#include <variant>

#include "cg/geometric-miscellaneous.hpp"
#include "fmt/format.h"

TEST_CASE("orientation", "[geometric-miscellaneous]")
{
	REQUIRE(cg::compute_orientation(
		{ 0.0, 0.0 },
		{ 1.0, 2.0 },
		{ 2.0, 4.0 }
	) == cg::orientation::COLLINEAR);

	REQUIRE(cg::compute_orientation(
		{ 3.0, 1.5 },
		{ 3.2, 2.0 },
		{ 3.0, 3.0 }
	) == cg::orientation::LEFT);

	REQUIRE(cg::compute_orientation(
		{ 0., 0. },
		{ -1., -1. },
		{ -1, 0. }
	) == cg::orientation::RIGHT);

}

TEST_CASE("between", "[geometric-miscellaneous]")
{
	REQUIRE(!cg::is_between({ 0.0, 0.0 }, { 1.0, 2.0 }, { 2.0, 4.0 }));

	REQUIRE(cg::is_between({ 0., 0. }, { 1.0, 2.0 }, { 0.5, 1.0 }));

	REQUIRE(cg::is_between({ 0., 0. }, { 0., 1. }, { 0., 0.4 }));
}

TEST_CASE("intersects", "[geometric-miscellaneous]")
{
	REQUIRE(cg::intersects(
			{{0., 0.}, {1., 2.}},
			{{.5, 1.}, {3., 3.}}
		)
	);

	REQUIRE(cg::intersects(
			{{0., 0.}, {1., 2.}},
			{{3., 3.}, {.5, 1.}}
		)
	);

	REQUIRE(cg::intersects(
			{{0., 0.}, {1., 1.}},
			{{0., 1.}, {1., 0.}}
		)
	);

	REQUIRE(cg::intersects(
			{{0., 0.}, {1., 1.}},
			{{1., -1.}, {1., 2.}}
		)
	);

	REQUIRE(!cg::intersects(
			{{0., 0.}, {1., 1.}},
			{{0., -1.}, {1., 0.}}
		)
	);
}

TEST_CASE("intersection", "[geometric-miscellaneous]")
{

	REQUIRE(cg::segment2d{{0.0, 1.1}, {2.2, 3.3}} ==
		cg::segment2d{{0.0, 1.1}, {2.2, 3.3}});

	REQUIRE(cg::segment2d{{0.1, 1.1}, {2.2, 3.3}} !=
		cg::segment2d{{0.0, 1.1}, {2.2, 3.3}});

	auto test_intersection = [](auto&& opt, auto&& value)
	{
		using type = std::decay_t<decltype(value)>;
		REQUIRE(opt.has_value());
		auto intersection = opt.value();
		REQUIRE(std::holds_alternative<type>(intersection));
		auto p = std::get<type>(intersection);
		REQUIRE(p == value);
	};

	test_intersection(cg::compute_intersection(
			{{0., 0.}, {1., 2.}},
			{{.5, 1.}, {3., 3.}}
		), glm::vec2{ .5, 1. }
	);

	test_intersection(cg::compute_intersection(
			{{0., 0.}, {1., 2.}},
			{{3., 3.}, {.5, 1.}}
		), glm::vec2{ .5, 1. }
	);

	test_intersection(cg::compute_intersection(
			{{0., 0.}, {1., 1.}},
			{{0., 1.}, {1., 0.}}
		), glm::vec2{ 0.5, 0.5 }
	);

	test_intersection(cg::compute_intersection(
			{{0., 0.}, {1., 1.}},
			{{1., -1.}, {1., 2.}}
		), glm::vec2{ 1., 1. }
	);

	REQUIRE(! cg::compute_intersection(
			{{0., 0.}, {1., 1.}},
			{{0., -1.}, {1., 0.}}
		).has_value()
	);

	test_intersection(cg::compute_intersection(
			{{0., 0.}, {1., 1.}},
			{{.5, .5}, {2., 2.}}
		), cg::segment2d{ {0.5, 0.5}, {1., 1.} }
	);

	SECTION("intersections of list 3"){
		std::array const seg{
			cg::segment2d{{-200., 700.}, {400., -300.}},
			cg::segment2d{{-100., 100.}, {400.,  600.}},
			cg::segment2d{{ 400., 300.}, {1000., 300.}},
		};

		struct{
			cg::segment2d const top{{    0., 600. }, {800., 600.}};
			cg::segment2d const bottom{{ 0., 0. },   {800., 0.}};
			cg::segment2d const left{{   0., 0. },   {0., 600.}};
			cg::segment2d const right{{  800., 0. }, {800., 600.}};
		}scr;

		auto extract_vec = [](auto&& opt)
		{
			return std::get<glm::vec2>(opt.value());
		};

		auto const [ a, b ]  = std::make_tuple(
			extract_vec(cg::compute_intersection(seg[0], scr.left)),
			extract_vec(cg::compute_intersection(seg[0], scr.bottom))
		);

		INFO(fmt::format(
			"seg[0] cliped = ({}, {}) -> ({}, {})",
			a.x, a.y, b.x, b.y
		));

		REQUIRE(false);

	}
}
