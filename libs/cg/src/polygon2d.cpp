#include "cg/polygon2d.hpp"
#include <range/v3/action/push_back.hpp>

namespace cg{

polygon2d operator*(polygon2d const& p, cg::scalar s)
{
	auto multiply_by_s = [s](auto&& x){ return x*static_cast<float>(s); };
	return p | vws::transform(multiply_by_s) | rgs::to<polygon2d>();
}

polygon2d operator*(cg::scalar s, polygon2d const& p)
{
	return p*s;
}

polygon2d operator+(polygon2d const& p, glm::vec2 const& v)
{
	return p
		| vws::transform([&v](auto&& pi){ return pi + v; })
		| rgs::to<polygon2d>();
}

polygon2d operator-(polygon2d const& p, glm::vec2 const& v)
{
	return p + (-v);
}

} // end of namespace cg
