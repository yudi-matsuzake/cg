#include "cg/geometric-miscellaneous.hpp"
#include "cg/segment2d.hpp"
#include <stdexcept>

namespace cg{

orientation compute_orientation(
	glm::vec2 const& a,
	glm::vec2 const& b,
	glm::vec2 const& c)
{
	auto compute_area = [](
		glm::vec2 const& a,
		glm::vec2 const& b,
		glm::vec2 const& c)
	{
		return	(b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y);
	};

	if(auto area = compute_area(a, b, c); area > 0.0)
		return orientation::LEFT;
	else if(area < 0.0)
		return orientation::RIGHT;
	return orientation::COLLINEAR;
}

bool is_between(glm::vec2 const& a, glm::vec2 const& b, glm::vec2 const& c)
{
	if(compute_orientation(a, b, c) != orientation::COLLINEAR)
		return false;

	if(a.x != b.x){
		return ((a.x <= c.x) && (c.x <= b.x))
			|| ((a.x >= c.x) && (c.x >= b.x));
	}else{
		return ((a.y <= c.y) && (c.y <= b.y))
			|| ((a.y >= c.y) && (c.y >= b.y));
	}
}

bool is_between(segment2d const& s, glm::vec2 const& p)
{
	return is_between(s.first, s.second, p);
}

bool is_colinear(glm::vec2 const& a, glm::vec2 const& b, glm::vec2 const& c)
{
	return compute_orientation(a, b, c) == orientation::COLLINEAR;
}

bool is_left(glm::vec2 const& a, glm::vec2 const& b, glm::vec2 const& c)
{
	return compute_orientation(a, b, c) == orientation::LEFT;
}

bool is_right(glm::vec2 const& a, glm::vec2 const& b, glm::vec2 const& c)
{
	return compute_orientation(a, b, c) == orientation::RIGHT;
}

bool is_colinear(segment2d const& s, glm::vec2 const& p)
{
	return is_colinear(s.first, s.second, p);
}

bool is_left(segment2d const& s, glm::vec2 const& p)
{
	return is_left(s.first, s.second, p);
}

bool is_right(segment2d const& s, glm::vec2 const& p)
{
	return is_right(s.first, s.second, p);
}


bool intersects(cg::segment2d const& a, cg::segment2d const& b)
{
	auto is_colinear_and_between = [](auto&& p, auto&& q, auto&& r)
	{
		return is_colinear(p, q, r) && is_between(p, q, r);
	};

	if(is_colinear_and_between(a.first, a.second, b.first)
		|| is_colinear_and_between(a.first, a.second, b.second)
		|| is_colinear_and_between(b.first, b.second, a.first)
		|| is_colinear_and_between(b.first, b.second, a.second)){
		return true;
	}

	return (is_left(a.first, a.second, b.first)
			^ is_left(a.first, a.second, b.second)) &&

		(is_left(b.first, b.second, a.first)
			^ is_left(b.first, b.second, a.second));
}

static auto compute_segment_intersection(segment2d const& a, segment2d const& b)
{
	auto bf = is_between(a, b.first);
	auto bs = is_between(a, b.second);

	if(bf && bs){
		return b;
	}else if(bf){
		if(is_between(b, a.first))
			return segment2d{ b.first, a.first };
		else
			return segment2d{ b.first, a.second };
	}else if(bs){
		if(is_between(b, a.first))
			return segment2d{ b.second, a.first };
		else
			return segment2d{ b.second, a.second };
	}

	return a;
}

static auto compute_point_intersection(segment2d const& a, segment2d const& b)
{
	// let
	auto p = a.first;
	auto r = a.second - a.first;
	auto q = b.first;
	auto s = b.second - b.first;

	// the intersection point is u = (q - p) x r / (r x s)
	// where v x w is a 2d cross product cross(v, w) is defined as
	auto cross = [](glm::vec2 const& v, glm::vec2 const& w)
	{
		return v.x * w.y - v.y * w.x;
	};

	auto nu = cross(q - p, r);
	auto nt = cross(q - p, s);
	auto d = cross(r, s);

	if(d == 0.0 && nu == 0.0){
		throw std::logic_error(
			"point intersection computation error: "
			"the segments must not be collinear at this point\n"
		);
	}else if(d == 0.0){
		throw std::logic_error(
			"point intersection computation error: "
			"the segments are parallel\n"
		);
	}

	auto u = nu / d;
	auto t = nt / d;

	return p + t*r;
}

intersection_between_seg2d compute_intersection(
	segment2d const& a,
	segment2d const& b)
{
	if(!intersects(a, b))
		return std::nullopt;

	if(is_colinear(a, b.first) && is_colinear(a, b.second))
		return compute_segment_intersection(a, b);

	return compute_point_intersection(a, b);
}

} // end of namespace cg
