#include "cg/inputs.hpp"
#include "fmt/format.h"
#include "cg/window.hpp"
#include "cg/buffers.hpp"
#include "cg/vao.hpp"
#include "cg/shader.hpp"
#include "cg/program.hpp"
#include <GLFW/glfw3.h>
#include <cstdlib>

class hello_triangle : public cg::window{
private:

static constexpr auto vs_shader = std::array{R"__(
#version 330 core

layout (location = 0) in vec3 pos;

void main()
{
	gl_Position = vec4(pos, 1.f);
}
)__"};

static constexpr auto fs_shader = std::array{R"__(
#version 330 core

out vec4 frag_color;

void main()
{
	frag_color = vec4(1.0f, 0.0f, 0.0f, 1.0f);
}
)__"};

	static constexpr auto list_of_vertices()
	{
		return std::array{
			// first triangle
			 0.0f,  0.5f, 0.0f,
			-0.5f, -0.5f, 0.0f,
			 0.5f, -0.5f, 0.0f
		};
	}


	auto make_program()
	{
		cg::vertex_shader vs;
		cg::fragment_shader fs;

		vs.set_source(vs_shader);
		fs.set_source(fs_shader);

		vs.compile();
		fs.compile();

		cg::program p;
		p.attach_shader(vs);
		p.attach_shader(fs);
		p.link();

		return p;
	}

public:
	hello_triangle()
		: cg::window(512, 512, "Hello triangle"),
		  program(make_program())
	{}

	auto run()
	{
		vao.bind();
		vbo.bind();

		constexpr auto vertices = list_of_vertices();
		vbo.set_data(vertices.data(), vertices.size());
		vao.set_attribute_layout({ cg::vao::attr<float>(3) });


		while(!this->should_close()){

			if(this->is_key_pressed(GLFW_KEY_Q)
				|| this->is_key_pressed(GLFW_KEY_ESCAPE))
				this->should_close(true);

			auto [ scr_w, scr_h ] = this->get_framebuffer_size();
			GL(glViewport(0, 0, scr_w, scr_h));

			GL(glClearColor(1.f, 1.f, 1.f, 1.f));
			GL(glClear(GL_COLOR_BUFFER_BIT));
			program.use();
			vao.bind();

			GL(glDrawArrays(GL_TRIANGLES, 0, vertices.size() / 3));

			this->swap_buffers();
			this->poll_events();
		}

		return EXIT_SUCCESS;
	}

protected:
	cg::program program;
	cg::vao vao;
	cg::vbo vbo;
};

int main(int const argc, char const* const* const argv)
{
	hello_triangle app;
	return app.run();
}
