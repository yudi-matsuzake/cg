#include "fmt/format.h"
#include "cg/window.hpp"
#include "cg/buffers.hpp"
#include "cg/vao.hpp"
#include "cg/shader.hpp"
#include "cg/program.hpp"
#include "cg/bezier-curve.hpp"

#include <GLFW/glfw3.h>
#include <algorithm>
#include <cstdlib>
#include <optional>
#include <range/v3/algorithm/min_element.hpp>
#include <range/v3/view/enumerate.hpp>
#include <range/v3/view/generate.hpp>

namespace rgs = ranges;
namespace vws = ranges::views;
namespace acs = ranges::actions;

class bezier : public cg::window{
private:

static constexpr auto vs_shader = std::array{R"__(
#version 330 core

layout (location = 0) in vec2 pos;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;

void main()
{
	gl_Position = u_projection*u_view*u_model*vec4(pos, 0.0, 1.0);
}
)__"};

static constexpr auto fs_shader = std::array{R"__(
#version 330 core

out vec4 frag_color;

uniform vec4 u_color;

void main()
{
	frag_color = u_color;
}
)__"};

	auto make_program()
	{
		cg::vertex_shader vs;
		cg::fragment_shader fs;

		vs.set_source(vs_shader);
		fs.set_source(fs_shader);

		vs.compile();
		fs.compile();

		cg::program p;
		p.attach_shader(vs);
		p.attach_shader(fs);
		p.link();

		return p;
	}

	struct{
		float width = 0.0;
		float height = 0.0;
	} scr;

	struct{
		glm::vec2 pos;
	} cursor;

	template <class T>
	void draw_array(
		T const* data,
		size_t size,
		int32_t type,
		glm::vec4 const& color)
	{
		vao.bind();
		vbo.set_data(data, size, GL_DYNAMIC_DRAW);

		program.use();
		vao.bind();
		vbo.bind();

		auto projection = glm::ortho(0.f, scr.width, 0.f, scr.height);
		program.set_uniform("u_model",		glm::mat4(1.f));
		program.set_uniform("u_view",		glm::mat4(1.f));
		program.set_uniform("u_projection",	projection);
		program.set_uniform("u_color",		color);

		GL(glDrawArrays(type, 0, size / 2));
	}

	auto get_cursor_screen_point()
	{
		auto m = this->get_cursor_vector();
		m.y = scr.height - m.y;
		return m;
	}

	auto find_nearest_point_curve(glm::vec2 const& p)
	{
		auto const dist_to_p = [&p](auto&& q)
		{
			return glm::distance(q, p);
		};

		auto const t_comp = [](auto&& a, auto&& b)
		{
			return std::get<1>(a) < std::get<1>(b);
		};

		auto const distances = bezier_curve
			| vws::transform(dist_to_p)
			| vws::enumerate;

		return *rgs::min_element(distances, t_comp);
	}

	auto update_index_to_change()
	{
		auto const m = cursor.pos;

		if(!index_to_change.has_value()){
			auto [ index, d ] = find_nearest_point_curve(m);

			if(d <= 50.f)
				index_to_change = index;
		}

	}

	auto update_state()
	{
		if(this->is_key_pressed(GLFW_KEY_Q)
			|| this->is_key_pressed(GLFW_KEY_ESCAPE))
			this->should_close(true);

		std::tie(scr.width, scr.height) = this->get_framebuffer_size();

		cursor.pos = get_cursor_screen_point();
	}

public:
	bezier()
		: cg::window(512, 512, "bezier"),
		  program(make_program())
	{
		GL(glEnable(GL_BLEND));
		GL(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
		GL(glEnable(GL_LINE_SMOOTH));
		GL(glHint(GL_LINE_SMOOTH_HINT, GL_NICEST));
	}

	void on_mouse_button(const cg::mouse_button_input& input) override
	{
		if(input.button == GLFW_MOUSE_BUTTON_LEFT
			&& input.action == GLFW_RELEASE){
			auto m = cursor.pos;
			bezier_curve.emplace_back(m);
		}else if(input.button == GLFW_MOUSE_BUTTON_RIGHT){
			if(input.action == GLFW_RELEASE){
				index_to_change = std::nullopt;
			}else if(input.action == GLFW_PRESS){
				if(bezier_curve.size())
					update_index_to_change();
			}
		}
	}

	auto run()
	{
		vao.bind();
		vbo.bind();
		vao.set_attribute_layout({ cg::vao::attr<float>(2) });

		std::vector<float> points;
		std::vector<float> curve;

		auto const curve_points = 100.f;
		auto const step = 1.f/100.f;

		while(!this->should_close()){

			update_state();

			points.clear();
			curve.clear();
			for(auto&& p : bezier_curve){
				points.emplace_back(p.x);
				points.emplace_back(p.y);
			}

			for(auto t = 0.f; t<1.f; t+=step){
				auto p = bezier_curve(t);
				curve.emplace_back(p.x);
				curve.emplace_back(p.y);
			}
			
			if(index_to_change.has_value())
				bezier_curve.at(index_to_change.value()) = cursor.pos;

			GL(glViewport(0, 0, scr.width, scr.height));

			GL(glClearColor(1.f, 1.f, 1.f, 1.f));
			GL(glClear(GL_COLOR_BUFFER_BIT));

			draw_array(
				curve.data(),
				curve.size(),
				GL_LINE_STRIP,
				glm::vec4{ 0.f, 0.f, 0.f, 1.f }
			);

			GL(glPointSize(5.f));
			draw_array(
				points.data(),
				points.size(),
				GL_POINTS,
				glm::vec4{ 1.f, 0.f, 0.f, 1.f }
			);

			this->swap_buffers();
			this->poll_events();
		}

		return EXIT_SUCCESS;
	}

protected:
	cg::program program;
	cg::vao vao;
	cg::vbo vbo;

	cg::bezier_curve<2> bezier_curve;

	std::optional<uint64_t> index_to_change = std::nullopt;
};

int main(int const argc, char const* const* const argv)
{
	bezier app;
	return app.run();
}
