#include "cg/inputs.hpp"
#include "fmt/format.h"
#include "cg/window.hpp"
#include <GLFW/glfw3.h>
#include <cstdlib>

constexpr auto vs_shader = std::array{R"__(
#version 330 core

layout (location = 0) in vec3 pos;

void main()
{
	gl_Position = vec4(pos, 1.f);
}
)__"};

constexpr auto fs_shader = std::array{R"__(
#version 330 core

out vec4 frag_color;

void main()
{
	frag_color = vec4(1.0f, 0.0f, 0.0f, 1.0f);
}
)__"};

static void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	GL(glViewport(0, 0, width, height));
}

constexpr auto list_of_vertices()
{
	return std::array{
		// first triangle
		 0.0f,  0.5f, 0.0f,
		-0.5f, -0.5f, 0.0f,
		 0.5f, -0.5f, 0.0f
	};
}

/*
 * check compilation and throw an exception in case
 * failed compilation
 */
static void check_shader_compilation(uint32_t id)
{
	int32_t success = 0;
	GL(glGetShaderiv(id, GL_COMPILE_STATUS, &success));

	if(!success){
		int32_t infolog_length = 0;
		GL(glGetShaderiv(
			id,
			GL_INFO_LOG_LENGTH,
			&infolog_length
		));

		std::vector<char> v(static_cast<unsigned>(infolog_length));

		std::cerr << infolog_length << '\n';

		GL(glGetShaderInfoLog(
			id,
			infolog_length,
			NULL,
			v.data()
		));

		throw std::runtime_error(v.data());
	}
}

/*
 * check linkage status and throw an exception in case
 * failed linkage
 */
static void check_program_linkage(uint32_t id)
{
	int32_t success = 0;
	GL(glGetProgramiv(id, GL_LINK_STATUS, &success));

	if(!success){
		int32_t infolog_length = 0;
		GL(glGetProgramiv(
			id,
			GL_INFO_LOG_LENGTH,
			&infolog_length
		));

		std::vector<char> v(static_cast<unsigned>(infolog_length));

		GL(glGetProgramInfoLog(
			id,
			infolog_length,
			NULL,
			v.data()
		));

		throw std::runtime_error(v.data());
	}
}

int main(int const argc, char const* const* const argv)
{
	cg::window w(512, 512, "Red polygon");
	glfwSetFramebufferSizeCallback(w.ptr(), framebuffer_size_callback);

	GL(glViewport(0, 0, 512, 512));
	GL(glEnable(GL_BLEND));
	GL(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

	uint32_t vao, vbo;
	GL(glGenVertexArrays(1, &vao));
	GL(glGenBuffers(1, &vbo));

	GL(glBindVertexArray(vao));
	GL(glBindBuffer(GL_ARRAY_BUFFER, vbo));

	constexpr auto vertices = list_of_vertices();
	GL(glBufferData(
		GL_ARRAY_BUFFER,
		sizeof(vertices),
		vertices.data(),
		GL_STATIC_DRAW
	));

	GL(glVertexAttribPointer(
		0, 3,
		GL_FLOAT, GL_FALSE,
		3*sizeof(float), nullptr
	));
	GL(glEnableVertexAttribArray(0));

	int32_t vs, fs;
	GL(vs = glCreateShader(GL_VERTEX_SHADER));
	GL(fs = glCreateShader(GL_FRAGMENT_SHADER));

	GL(glShaderSource(vs, vs_shader.size(), vs_shader.data(), nullptr));
	GL(glShaderSource(fs, fs_shader.size(), fs_shader.data(), nullptr));

	GL(glCompileShader(vs));
	GL(glCompileShader(fs));

	check_shader_compilation(vs);
	check_shader_compilation(fs);

	int32_t program;
	GL(program = glCreateProgram());

	GL(glAttachShader(program, vs));
	GL(glAttachShader(program, fs));
	GL(glLinkProgram(program));
	check_program_linkage(program);

	GL(glDeleteShader(vs));
	GL(glDeleteShader(fs));

	while(!w.should_close()){

		if(w.is_key_pressed(GLFW_KEY_Q)
			|| w.is_key_pressed(GLFW_KEY_ESCAPE))
			w.should_close(true);

		GL(glClearColor(1.f, 1.f, 1.f, 1.f));
		GL(glClear(GL_COLOR_BUFFER_BIT));

		GL(glUseProgram(program));
		GL(glBindVertexArray(vao));

		GL(glDrawArrays(GL_TRIANGLES, 0, vertices.size() / 3));

		w.swap_buffers();
		w.poll_events();
	}

	GL(glDeleteVertexArrays(1, &vao));
	GL(glDeleteBuffers(1, &vbo));
	GL(glDeleteProgram(program));

	return EXIT_SUCCESS;
}
