#pragma once

#include <array>

#include "cg/misc.hpp"

namespace gmt{

/**
  * translates the transformation matrix `m` with the displacement
  * vector `v`
  */
auto translate(glm::mat4 const& m, glm::vec3 const& v)
{
	auto r = m;
	r[3][0] += v.x;
	r[3][1] += v.y;
	r[3][2] += v.z;
	return r;
}

/**
  * scale the transformation matrix `m` in each axis
  * by `v.x`, `v.y`, and `v.z`. 
  */
auto scale(glm::mat4 const& m, glm::vec3 const& v)
{
	auto r = m;
	r[0][0] *= v.x;
	r[1][1] *= v.y;
	r[2][2] *= v.z;
	return r;
}

/**
  * rotates the transform matrix `m` by `angle` radians
  * around the x axis
  */
auto rotate_x(glm::mat4 const& m, float angle)
{
	auto cos = std::cos(angle);
	auto sin = std::sin(angle);

	glm::mat4 r{
		1., 0., 0., 0.,
		0., cos, sin, 0.,
		0., -sin, cos, 0.,
		0., 0., 0., 1.
	};

	return m*r;
}

/**
  * rotates the transform matrix `m` by `angle` radians
  * around the y axis
  */
auto rotate_y(glm::mat4 const& m, float angle)
{
	auto cos = std::cos(angle);
	auto sin = std::sin(angle);

	glm::mat4 r{
		cos,	0.,	sin,	0.,
		0.,	1.,	0.,	0.,
		-sin,	0.,	cos,	0.,
		0.,	0.,	0.,	1.
	};

	return m*r;
}

/**
  * rotates the transform matrix `m` by `angle` radians
  * around the z axis
  */
auto rotate_z(glm::mat4 const& m, float angle)
{
	auto cos = std::cos(angle);
	auto sin = std::sin(angle);

	glm::mat4 r{
		cos,	sin,	0,	0,
		-sin,	cos,	0.,	0.,
		0.,	0.,	1,	0.,
		0.,	0.,	0.,	1.
	};

	return m*r;
}


} // end of namespace gmt
