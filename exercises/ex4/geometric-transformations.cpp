#include <chrono>
#include <fstream>
#include <filesystem>
#include <cstdlib>

#include "fmt/format.h"

#include "cg/inputs.hpp"
#include "cg/window.hpp"
#include "cg/buffers.hpp"
#include "cg/vao.hpp"
#include "cg/shader.hpp"
#include "cg/program.hpp"
#include "cg/misc.hpp"

#include "gmt.hpp"

namespace chrono = std::chrono;
namespace fs = std::filesystem;

namespace rgs = ranges;
namespace vws = ranges::views;
namespace acs = ranges::actions;

constexpr auto vs_shader = std::array{R"__(
#version 330 core

layout (location = 0) in vec3 l_pos;
layout (location = 1) in vec3 l_normal;

out vec3 normal;
out vec3 frag_pos;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;

void main()
{
	normal = mat3(transpose(inverse(u_model))) * l_normal;
	frag_pos = vec3(u_model * vec4(l_pos, 1.0));
	gl_Position = u_projection * u_view * u_model * vec4(l_pos, 1.f);
}
)__"};

constexpr auto fs_shader = std::array{R"__(
#version 330 core
out vec4 frag_color;

in vec3 normal;
in vec3 frag_pos;

uniform vec3 u_light_pos;
uniform vec3 u_view_pos;
uniform vec3 u_light_color;
uniform vec3 u_object_color;

void main()
{
	// ambient
	float ambient_strength = 0.1;
	vec3 ambient = ambient_strength * u_light_color;

	// diffuse
	vec3 norm = normalize(normal);
	vec3 lightDir = normalize(u_light_pos - frag_pos);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * u_light_color;

	// specular
	float specular_strength = 0.5;
	vec3 view_dir = normalize(u_view_pos - frag_pos);
	vec3 reflect_dir = reflect(-lightDir, norm);  
	float spec = pow(max(dot(view_dir, reflect_dir), 0.0), 32);
	vec3 specular = specular_strength * spec * u_light_color;  

	vec3 result = (ambient + diffuse + specular) * u_object_color;
	frag_color = vec4(result, 1.0);
} 
)__"};

auto load_from_obj(fs::path const& obj_file)
{
	std::ifstream obj_stream(obj_file);

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<int32_t> vertex_indices;
	std::vector<int32_t> normal_indices;

	std::string line;
	while(std::getline(obj_stream, line)){

		std::stringstream ss(line);

		auto get_next_vertex = [&ss](auto&& line)
		{
			glm::vec3 v;
			ss >> v.x;
			ss >> v.y;
			ss >> v.z;
			return v;
		};

		std::string str;
		ss >> str;

		if(str == "v"){
			vertices.emplace_back(get_next_vertex(line));
		}else if(str == "vn"){
			normals.emplace_back(get_next_vertex(line));
		}else if(str == "f"){
			auto bar_count = 0;
			for(auto i=0; i<line.size(); ++i){
				auto const c = line[i];

				if(std::isdigit(c)){
					char const* first_digit = line.data() + i;
					char* last;

					int32_t const index = std::strtol(
						first_digit,
						&last,
						10
					) - 1;

					if(bar_count == 0)
						vertex_indices.emplace_back(index);
					/* else if(bar_count == 2) */
					/* 	texture */
					else if(bar_count == 2)
						normal_indices.emplace_back(index);

					i += last - first_digit - 1;
				}else if(c == '/'){
					bar_count++;
				}else{
					bar_count = 0;
				}
			}
		}
	}

	std::vector<float> data;
	data.reserve(vertex_indices.size()*6);

	auto faces = vws::zip(vertex_indices, normal_indices) | vws::chunk(3);

	for(auto&& f : faces){

		auto mean = glm::vec3(0.f, 0.f, 0.f);

		for(auto&& [ vidx, nidx ] : f)
			mean += normals[nidx];

		mean /= 3.;

		for(auto&& [ vidx, nidx ] : f){
			auto& v = vertices[vidx];
			data.emplace_back(v.x);
			data.emplace_back(v.y);
			data.emplace_back(v.z);
			data.emplace_back(mean.x);
			data.emplace_back(mean.y);
			data.emplace_back(mean.z);
		}
	}

	return data;
}

struct mesh {
	cg::vao vao;
	cg::vbo vbo;

	glm::mat4 model = glm::mat4(1.f);
	glm::vec3 color = glm::vec3(1.f, 0.f, 0.f);

	uint32_t n_elements = 0;

	mesh(fs::path const& path)
	{
		vao.bind();
		auto data = load_from_obj(path);
		vbo.set_data(data.data(), data.size());
		vao.set_attribute_layout({
			cg::vao::attr<float>(3),
			cg::vao::attr<float>(3)
		});

		n_elements = data.size() / 6;
	}

	void bind() const
	{
		vao.bind();
		vbo.bind();
	}
};

class geometric_transformations : public cg::window{
private:

	struct input{
		uint32_t target_mesh_index	= 1;

		struct{
			chrono::milliseconds cooldown{500};
			bool value = true;

			void set_status(bool v)
			{
				static auto last = chrono::steady_clock::now();
				static auto first = true;

				auto now = chrono::steady_clock::now();

				if(first){
					first = false;
					value = v;
					last = now;
				}else{
					auto const d = now - last;
					if(d >= cooldown){
						value = v;
						last = now;
					}
				}
			}
		}camera_mov;

		bool left	= false;
		bool right	= false;
		bool up		= false;
		bool down	= false;
		bool back	= false;
		bool front	= false;

		struct{
			struct{
				bool up		= false;
				bool down	= false;
			}x, y, z;
		}scale;

		struct{
			bool x = false;
			bool y = false;
			bool z = false;
		}rotate;

		struct{
			bool left	= false;
			bool right	= false;
			bool up		= false;
			bool down	= false;
			bool back	= false;
			bool front	= false;
		}translate;
	};

	struct{
		glm::vec3 position = glm::vec3(0.f, 0.f, 0.f);

		glm::mat4 view()
		{
			glm::mat4 v(1.f);
			return gmt::translate(v, -position);
		}

	}camera;

	auto process_inputs(input const& last)
	{
		input input{};
		input.camera_mov = last.camera_mov;
		input.target_mesh_index = last.target_mesh_index;

		if(this->is_key_pressed(GLFW_KEY_ESCAPE))
			this->should_close(true);

		if(this->is_key_pressed(GLFW_KEY_C))
			input.camera_mov.set_status(!input.camera_mov.value);

		if(this->is_key_pressed(GLFW_KEY_S))
			input.rotate.x = true;
		if(this->is_key_pressed(GLFW_KEY_Q))
			input.rotate.y = true;
		if(this->is_key_pressed(GLFW_KEY_A))
			input.rotate.z = true;

		if(this->is_key_pressed(GLFW_KEY_X)){
			if(this->is_key_pressed(GLFW_KEY_LEFT_SHIFT))
				input.scale.x.up = true;
			else
				input.scale.x.down = true;
		}

		if(this->is_key_pressed(GLFW_KEY_Y)){
			if(this->is_key_pressed(GLFW_KEY_LEFT_SHIFT))
				input.scale.y.up = true;
			else
				input.scale.y.down = true;
		}

		if(this->is_key_pressed(GLFW_KEY_Z)){
			if(this->is_key_pressed(GLFW_KEY_LEFT_SHIFT))
				input.scale.z.up = true;
			else
				input.scale.z.down = true;
		}

		if(input.camera_mov.value){
			if(this->is_key_pressed(GLFW_KEY_LEFT))
				input.left = true;

			if(this->is_key_pressed(GLFW_KEY_RIGHT))
				input.right = true;

			if(this->is_key_pressed(GLFW_KEY_UP)){
				if(this->is_key_pressed(GLFW_KEY_LEFT_CONTROL))
					input.front = true;
				else
					input.up = true;
			}

			if(this->is_key_pressed(GLFW_KEY_DOWN)){
				if(this->is_key_pressed(GLFW_KEY_LEFT_CONTROL))
					input.back = true;
				else
					input.down = true;
			}
		}else{
			if(this->is_key_pressed(GLFW_KEY_LEFT))
				input.translate.left = true;

			if(this->is_key_pressed(GLFW_KEY_RIGHT))
				input.translate.right = true;

			if(this->is_key_pressed(GLFW_KEY_UP))
				input.translate.up = true;

			if(this->is_key_pressed(GLFW_KEY_DOWN))
				input.translate.down = true;

			if(this->is_key_pressed(GLFW_KEY_PERIOD))
				input.translate.front = true;

			if(this->is_key_pressed(GLFW_KEY_COMMA))
				input.translate.back = true;
		}

		for(auto k = 0; k<=9; ++k){
			if(is_key_pressed(GLFW_KEY_0 + k))
				input.target_mesh_index = k;
		}

		return input;
	}

	void update_camera(input const& input)
	{
		// update camera
		glm::vec3 mov(0.f, 0.f, 0.f);
		if(input.left)
			mov += glm::vec3(-1.f, 0.f, 0.f);
		if(input.right)
			mov += glm::vec3(1.f, 0.f, 0.f);
		if(input.up)
			mov += glm::vec3(0.f, 1.f, 0.f);
		if(input.down)
			mov += glm::vec3(0.f, -1.f, 0.f);
		if(input.front)
			mov += glm::vec3(0.f, 0.f, -1.f);
		if(input.back)
			mov += glm::vec3(0.f, 0.f, 1.f);

		camera.position += 5.f*delta*mov;
	}

	auto update_target_model(input const& input, glm::mat4 const& model)
	{
		auto m = model;

		if(input.rotate.x || input.rotate.y || input.rotate.z){
			if(input.rotate.x)
				m = gmt::rotate_x(m, glm::pi<float>()*delta);
			if(input.rotate.y)
				m = gmt::rotate_y(m, glm::pi<float>()*delta);
			if(input.rotate.z)
				m = glm::rotate(m, glm::pi<float>()*delta, glm::vec3{ 0., 0., 1. });
		}

		glm::vec3 translate(0.f, 0.f, 0.f);
		if(input.translate.left)
			translate.x -= 1.f;
		if(input.translate.right)
			translate.x += 1.f;
		if(input.translate.up)
			translate.y += 1.f;
		if(input.translate.down)
			translate.y -= 1.f;
		if(input.translate.front)
			translate.z += 1.f;
		if(input.translate.back)
			translate.z -= 1.f;

		m = gmt::translate(m, translate*delta);

		glm::vec3 scale(1.f, 1.f, 1.f);

		if(input.scale.x.up)
			scale.x += 0.1;
		if(input.scale.x.down)
			scale.x -= 0.1;
		if(input.scale.y.up)
			scale.y += 0.1;
		if(input.scale.y.down)
			scale.y -= 0.1;
		if(input.scale.z.up)
			scale.z += 0.1;
		if(input.scale.z.down)
			scale.z -= 0.1;

		m = gmt::scale(m, scale);

		return m;
	}

	auto load_meshes()
	{
		auto meshes = std::array{
			mesh("res/cube.obj"),
			mesh("res/sphere.obj"),
			mesh("res/tetrahedra.obj"),
			mesh("res/icosphere.obj"),
			mesh("res/monkey.obj"),
			mesh("res/torus.obj"),
			mesh("res/torus-knot.obj"),
			mesh("res/cone.obj"),
			mesh("res/cylinder.obj")
		};

		auto meshes_displacements = std::array{
			glm::vec3{ -1.f, +1.f, 0.f },
			glm::vec3{  0.f, +1.f, 0.f },
			glm::vec3{ +1.f, +1.f, 0.f },
			glm::vec3{ -1.f,  0.f, 0.f },
			glm::vec3{  0.f,  0.f, 0.f },
			glm::vec3{ +1.f,  0.f, 0.f },
			glm::vec3{ -1.f, -1.f, 0.f },
			glm::vec3{  0.f, -1.f, 0.f },
			glm::vec3{ +1.f, -1.f, 0.f }
		};

		for(auto i=0; i<meshes.size(); ++i){
			meshes[i].model = gmt::translate(
				meshes[i].model,
				meshes_displacements[i]*5.f
			);

			meshes[i].color =
				1.f/2.f*(meshes_displacements[i] +
				glm::vec3(1.f, 1.f, 1.f));
		}

		return meshes;
	}

	void draw_mesh(
		glm::mat4 const& projection,
		glm::mat4 const& view,
		mesh const& m)
	{
		program.use();
		m.bind();

		program.set_uniform("u_model", global_transform*m.model );
		program.set_uniform("u_view", view);
		program.set_uniform("u_projection", projection);
		program.set_uniform("u_light_pos", glm::vec3(3., 3., 3.));
		program.set_uniform("u_view_pos", glm::vec3(0., 0., 6.));
		program.set_uniform("u_light_color", glm::vec3(1., 1., 1.));
		program.set_uniform("u_object_color", m.color);

		GL(glDrawArrays(GL_TRIANGLES, 0, m.n_elements));
	}

public:
	geometric_transformations()
		: cg::window(1280, 720, "Geometric transformations"),
		program(make_program())
	{
		glfwSetFramebufferSizeCallback(this->ptr(), framebuffer_size_callback);

		GL(glEnable(GL_DEPTH_TEST));
		GL(glEnable(GL_BLEND));
		GL(glViewport(0, 0, 1280, 720));
		GL(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

	}

	auto run()
	{
		auto last_time = chrono::steady_clock::now();

		auto meshes = load_meshes();

		camera.position = glm::vec3(0.f, 0.f, 20.0f);

		input input{};


		while(!this->should_close()){
			auto const now = chrono::steady_clock::now();
			auto const d = now - last_time;
			delta = chrono::duration<float>(d).count();

			last_time = now;

			input = process_inputs(input);
			update_camera(input);

			auto& target_model = 
				(input.target_mesh_index == 0)
				? global_transform
				: meshes.at(input.target_mesh_index - 1).model;

			target_model = update_target_model(input, target_model);

			// create projection
			auto [ sw, sh ] = this->get_framebuffer_size();
			auto screen_width = static_cast<float>(sw);
			auto screen_height = static_cast<float>(sh);

			auto const projection = glm::perspective(
				glm::radians(45.f),
				screen_width / screen_height,
				.1f, 100.f
			);

			GL(glClearColor(0.f, 0.f, 0.f, 1.f));
			GL(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

			for(auto&& m : meshes){
				draw_mesh(
					projection,
					camera.view(),
					m
				);
			}

			this->swap_buffers();
			this->poll_events();
		}

		return EXIT_SUCCESS;
	}

private:
	cg::program program;

	float delta = 0.f;

	glm::mat4 global_transform = glm::mat4(1.f);

	static void framebuffer_size_callback(GLFWwindow* window, int width, int height)
	{
		GL(glViewport(0, 0, width, height));
	}

	static cg::program make_program()
	{
		cg::vertex_shader vs;
		cg::fragment_shader fs;

		vs.set_source(vs_shader);
		fs.set_source(fs_shader);

		vs.compile();
		fs.compile();

		cg::program p;
		p.attach_shader(vs);
		p.attach_shader(fs);
		p.link();

		return p;
	}
};

int main(int const argc, char const* const* const argv)
{

	geometric_transformations app;

	return app.run();
}
