#pragma once

#include <memory>
#include <QMainWindow>

#include "ui_mainwindow.h"

#include "cg/polygon2d.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT
signals:
	void polygonChanged(cg::polygon2d const& p);
	void fillColorChanged(QColor const& color);

public slots:
	void setFillColor();
	void setPolygonToSlideExample();
	void setPolygonToHorizontalLines();
	void setPolygonToTweezers();
	void setPolygonToHorizontalTweezers();
	void setPolygonToAliasing();
	void setPolygonToSelfIntersectingSpiral();

public:
	explicit MainWindow(QWidget *parent = nullptr);

private:
	cg::polygon2d adjustPolygonToScreen(cg::polygon2d const& poly);
	void updateLabelColor();

	std::unique_ptr<Ui::MainWindow> ui;
	QColor m_fill_color = Qt::black;
};
