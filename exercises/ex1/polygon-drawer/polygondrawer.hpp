#pragma once

#include <QWidget>
#include <QPainter>
#include <vector>
#include <QPoint>

#include "ui_polygondrawer.h"
#include "cg/polygon2d.hpp"

class PolygonDrawer : public QWidget
{
	Q_OBJECT

public slots:

	void setDrawBoundary(bool);
	void setPolygon(cg::polygon2d const& poly);
	void clearPolygon();
	void setFillColor(QColor const& color);

public:
	explicit PolygonDrawer(QWidget *parent = nullptr);

private:
	std::unique_ptr<Ui::PolygonDrawer> ui;
	cg::polygon2d m_poly;

	void paintEvent(QPaintEvent *event) override;
	void mouseReleaseEvent(QMouseEvent* e) override;
	void mousePressEvent(QMouseEvent* e) override;
	void mouseMoveEvent(QMouseEvent* e) override;

	void changeVertexBegin(glm::vec2 const& cursor);
	void changeVertexEnd();

	QColor m_fill_color = Qt::black;

	std::optional<uint64_t> m_chaging_vertex_idx = std::nullopt;
	bool m_change_vertex = false;

	bool m_draw_boundary = false;
};
