#include <algorithm>
#include <glm/ext/matrix_transform.hpp>
#include <glm/geometric.hpp>
#include <qnamespace.h>
#include <range/v3/algorithm/min_element.hpp>
#include <range/v3/view/enumerate.hpp>
#include <range/v3/view/subrange.hpp>
#include <range/v3/view/transform.hpp>
#include <unordered_map>
#include <set>
#include <limits>
#include <functional>
#include <range/v3/view/drop_exactly.hpp>

#include <QPalette>
#include <QMouseEvent>

#include "cg/geometric-miscellaneous.hpp"
#include "cg/polygon2d.hpp"
#include "cg/segment2d.hpp"
#include "util/util.hpp"
#include "polygondrawer.hpp"

#include "range/v3/all.hpp"

namespace rgs = ranges;
namespace vws = ranges::views;
namespace acs = ranges::actions;

struct ActiveSegmentSetCmp{
	bool operator()(cg::segment2d const& a, cg::segment2d const& b) const
	{
		return std::max(a.first.y, a.second.y) < std::max(b.first.y, b.second.y);
	}
};

using SegmentsToActivateTable = std::unordered_multimap<
	int64_t,
	cg::segment2d
>;

using ActiveSegmentSet = std::multiset<
	cg::segment2d,
	ActiveSegmentSetCmp
>;

PolygonDrawer::PolygonDrawer(QWidget *parent) :
	QWidget(parent),
	ui(std::make_unique<Ui::PolygonDrawer>())
{
	ui->setupUi(this);

	QPalette pal = palette();

	// set white background
	pal.setColor(QPalette::Window, Qt::white);
	this->setAutoFillBackground(true);
	this->setPalette(pal);
	this->show();
}

void PolygonDrawer::setDrawBoundary(bool t)
{
	m_draw_boundary = t;
	repaint();
}

void PolygonDrawer::setPolygon(cg::polygon2d const& poly)
{
	m_poly = poly;
	repaint();
}

void PolygonDrawer::clearPolygon()
{
	m_poly.clear();
	repaint();
}

void PolygonDrawer::setFillColor(QColor const& color)
{
	m_fill_color = color;
	repaint();
}

static auto computeClosestPointIndex(
	cg::polygon2d const& poly,
	glm::vec2 const& v)

	-> std::optional<int64_t>
{
	auto distance = [&v](glm::vec2 const& p)
	{
		return glm::distance(v, p);
	};

	auto cmp_dist = [](auto&& a, auto&& b)
	{
		return std::get<1>(a) < std::get<1>(b);
	};

	auto e_dist = poly | vws::transform(distance) | vws::enumerate;
	auto min_it = rgs::min_element(e_dist, cmp_dist);

	if(min_it == end(e_dist))
		return std::nullopt;

	auto&& [ idx, d ] = *min_it;
	return idx;
}

void PolygonDrawer::changeVertexBegin(glm::vec2 const& cursor)
{
	m_change_vertex = true;
	m_chaging_vertex_idx = computeClosestPointIndex(
		m_poly,
		cursor
	);
}

void PolygonDrawer::changeVertexEnd()
{
	m_change_vertex = false;
	m_chaging_vertex_idx = std::nullopt;
}

void PolygonDrawer::mousePressEvent(QMouseEvent* e)
{
	auto cursor = glm::vec2{ e->pos().x(), e->pos().y() };

	if(e->button() == Qt::LeftButton){
		m_poly.push_back(cursor);
		repaint();
	}else if(e->button() == Qt::RightButton){
		changeVertexBegin(cursor);
	}
}

void PolygonDrawer::mouseReleaseEvent(QMouseEvent* e)
{
	if(e->button() == Qt::RightButton){
		changeVertexEnd();
	}
}

void PolygonDrawer::mouseMoveEvent(QMouseEvent* e)
{
	if(m_change_vertex && m_chaging_vertex_idx.has_value()){
		glm::vec2 cursor{ e->pos().x(), e->pos().y() };
		m_poly[m_chaging_vertex_idx.value()] = cursor;
		repaint();
	}
}

template<class Scalar>
static auto round_upwards(Scalar x)
{
	return static_cast<int64_t>(std::ceil(x));
}

template<class Scalar>
static auto round_downwards(Scalar x)
{
	return static_cast<int64_t>(std::floor(x));
}

static auto computeIntegerBoundingBox(cg::polygon2d const& poly)
{
	auto inf = std::numeric_limits<cg::scalar>::infinity();
	glm::vec2 tl{ +inf, +inf };
	glm::vec2 br{ -inf, -inf };

	for(auto&& p : poly){
		if(p.x < tl.x) tl.x = p.x;
		if(p.y < tl.y) tl.y = p.y;
		if(p.x > br.x) br.x = p.x;
		if(p.y > br.y) br.y = p.y;
	}

	return std::tuple{
		round_downwards(tl.x), round_upwards(br.x),
		round_downwards(tl.y), round_upwards(br.y)
	};
}

static auto initializeSegmentsToActivate(
	cg::polygon2d const& poly, QRect const& r)
{
	SegmentsToActivateTable table;
	table.reserve(r.height());

	auto is_horizontal = [](cg::segment2d const& s)
	{
		return s.first.y == s.second.y;
	};

	for(auto&& s : cg::segments_view(poly)){

		if(!is_horizontal(s)){
			auto ymin = std::min(s.first.y, s.second.y);
			auto idx = round_downwards(ymin);
			table.emplace(ymin, s);
		}
	}

	return table;
}

static void polygonFillScanline(
	QPaintDevice* device,
	cg::polygon2d const& poly,
	QColor const& fill_color)
{
	QPainter painter(device);

	auto [ xmin, xmax, ymin, ymax ] = computeIntegerBoundingBox(poly);

	QRect r(xmin, ymin, xmax - xmin, ymax - ymin);
	painter.setPen(fill_color);

	/*
	 * initialize data structures
	 */
	auto segments_to_activate = initializeSegmentsToActivate(poly, r);

	ActiveSegmentSet active_segments;

	auto insert_active_segment = [&](auto&& pair)
	{
		auto&& [ k, s ] = pair;
		active_segments.insert(s);
	};

	auto still_active = [](auto y)
	{
		return [y](cg::segment2d const& s)
		{
			return std::max(s.first.y, s.second.y) > y;
		};
	};

	std::vector<cg::scalar> x_intersections;
	x_intersections.reserve(r.width() / 2);

	for(auto&& scan_y : vws::iota(ymin, ymax)){

		/*
		 * insert new segments that intersect the scanline
		 */
		auto&& [ beg, end ] = segments_to_activate.equal_range(scan_y);
		std::for_each(beg, end, insert_active_segment);

		/*
		 * remove segments that no longer intersect the scanline
		 */
		active_segments.erase(
			begin(active_segments),
			rgs::find_if(active_segments, still_active(scan_y))
		);

		/*
		 * build the scan line segment
		 */
		cg::segment2d scanline{
			{ r.x(), scan_y },
			{ r.x() + r.width(), scan_y }
		};

		/*
		 * compute intersections
		 */
		for(auto&& [i, s] : vws::enumerate(active_segments)){

			auto intersection = cg::compute_intersection(
				scanline,
				s
			);

			if(intersection.has_value()){
				auto p = std::get<glm::vec2>(intersection.value());
				x_intersections.emplace_back(p.x);
			}
		}

		acs::sort(x_intersections);

		/*
		 * draw line
		 */
		for(auto&& range : x_intersections | vws::chunk(2)){

			auto it		= begin(range);
			auto from	= round_upwards(*it++);
			auto to		= round_downwards(*it++);

			painter.drawLine(from, scan_y, to, scan_y);
		}

		x_intersections.clear();
	}
}

static void polygonDrawBoundary(
	QPaintDevice* device,
	cg::polygon2d const& poly,
	QColor const& color)
{
	QPainter painter(device);
	painter.setPen(color);

	auto make_xrouding = [](auto&& a, auto&& b)
		-> std::function<int64_t(float)>
	{
		if(a.y >= b.y)
			return round_downwards<float>;
		return round_upwards<float>;
	};

	auto make_yrouding = [](auto&& a, auto&& b)
		-> std::function<int64_t(float)>
	{
		if(a.x >= b.x)
			return round_upwards<float>;
		return round_downwards<float>;
	};


	for(auto&& [ a, b ] : cg::segments_view(poly)){
		auto xr = make_xrouding(a, b);
		auto yr = make_yrouding(a, b);
		painter.drawLine(xr(a.x), yr(a.y), xr(b.x), yr(b.y));
	}
}

/*
Paint events are sent to widgets that need to UPDATE or REPAINT themselves,
*/
void PolygonDrawer::paintEvent(QPaintEvent *)
{
	if(m_poly.size() <= 2) return;

	polygonFillScanline(this, m_poly, m_fill_color);

	if(m_draw_boundary)
		polygonDrawBoundary(this, m_poly, Qt::red);
}
