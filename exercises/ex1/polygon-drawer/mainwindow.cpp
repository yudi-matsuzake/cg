#include <QMouseEvent>

#include <QColorDialog>
#include <limits>

#include "cg/polygon2d.hpp"
#include "util/util.hpp"
#include "mainwindow.hpp"

namespace rgs = ranges;
namespace vws = ranges::views;
namespace acs = ranges::actions;

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(std::make_unique<Ui::MainWindow>())
{
	ui->setupUi(this);
	updateLabelColor();
}

static auto computeBoudingBox(cg::polygon2d const& poly)
{
	auto inf = std::numeric_limits<cg::scalar>::infinity();
	auto min_x = +inf;
	auto min_y = +inf;
	auto max_x = -inf;
	auto max_y = -inf;

	for(auto&& p : poly){
		if(p.x < min_x) min_x = p.x;
		if(p.x > max_x) max_x = p.x;
		if(p.y < min_y) min_y = p.y;
		if(p.y > max_y) max_y = p.y;
	}

	struct BoundingBox{
		cg::scalar const x = 0.0;
		cg::scalar const y = 0.0;
		cg::scalar const w = 0.0;
		cg::scalar const h = 0.0;

		auto center() const
		{
			return glm::vec2{ x + w/2., y + h/2. };
		}
	};

	return BoundingBox{ min_x, min_y, max_x - min_x, max_y - min_y };
}

cg::polygon2d MainWindow::adjustPolygonToScreen(cg::polygon2d const& poly)
{
	auto const tos = util::make_cast<cg::scalar>();

	auto const pd = ui->polygonDrawer;
	auto const pd_width	= tos(pd->width());
	auto const pd_height	= tos(pd->height());
	auto const pd_center	= glm::vec2{ pd_width/2., pd_height/2. };
	auto const pd_ratio	= pd_width / pd_height;

	auto const bd = computeBoudingBox(poly);
	auto const bd_ratio = bd.w / bd.h;

	auto const scale = [&]() -> float
	{
		if(bd_ratio > pd_ratio)
			return pd->width() / bd.w;

		return pd->height() / bd.h;
	}();

	return poly*scale + (pd_center - bd.center()*scale);
}

void MainWindow::setPolygonToSlideExample()
{
	static cg::polygon2d const slide_example{
		{100., 170.},
		{120., 140.},
		{130., 160.},
		{150., 160.},
		{170., 120.},
		{130., 100.},
		{100., 110.},
	};

	emit polygonChanged(adjustPolygonToScreen(slide_example));
}

void MainWindow::setPolygonToHorizontalLines()
{
	static cg::polygon2d const poly{
		{ 0., 2. }, { 1., 2. }, { 1., 0. },
		{ 2., 0. }, { 2., 2. }, { 3., 2. },
		{ 4., 0. }, { 5., 2. }, { 6., 2. },
		{ 6., 5. }, { 3., 5. }, { 5., 9. },
		{ 0., 9. }, { 2., 5. }, { 0., 5. },
		{ 0., 2. }, { 1., 2. }
	};
	emit polygonChanged(adjustPolygonToScreen(poly));
}

void MainWindow::setPolygonToTweezers()
{
	static cg::polygon2d const slide_example{
		{ 0., 2. }, { 1., 4. }, { 3., 3. },
		{ 2., 2. }, { 1., 3. }, { 1., 1. },
		{ 2., 2. }, { 3., 1. }, { 1., 0. }
	};
	emit polygonChanged(adjustPolygonToScreen(slide_example));
}

void MainWindow::setPolygonToAliasing()
{
	constexpr auto eps = 0.01;
	static cg::polygon2d const poly{
		{ 2.12398, 1.120938 }, { 0., eps }, {eps, 0. }
	};
	emit polygonChanged(adjustPolygonToScreen(poly));
}

void MainWindow::setPolygonToHorizontalTweezers()
{
	static cg::polygon2d const slide_example{
		{ 0., 0. }, { 2., 3. }, { 4., 0. },
		{ 2., 0. }, { 3., 1. }, { 1., 1. },
		{ 2., 0. }
	};
	emit polygonChanged(adjustPolygonToScreen(slide_example));
}

static cg::polygon2d generateSpiralPolygon(uint64_t n_turns, uint64_t n_vertices)
{
	auto tos = util::make_cast<cg::scalar>();
	cg::scalar turns = tos(n_turns);
	cg::scalar vertices = tos(n_vertices);

	cg::polygon2d poly;


	float const siz_step = 1./turns;
	float siz = siz_step;
	glm::vec2 v{ siz_step, 0.0 };

	float const rot_step = (2.f*glm::pi<float>()
		+ 1./n_vertices) / (n_vertices);

	for(auto&& turn : vws::iota(0UL, n_turns)){
		for(auto&& i : vws::iota(0UL, n_vertices)){
			poly.push_back(v);
			v = glm::rotate(v, rot_step);
		}

		siz += siz_step;
		v = v/glm::length(v) * siz;
	}

	return poly;
}

void MainWindow::setPolygonToSelfIntersectingSpiral()
{
	auto n_turns = ui->spiralNumberOfTurns->value();
	auto n_vertices = ui->spiralNumberOfVertexPerTurn->value();
	auto poly = generateSpiralPolygon(n_turns, n_vertices);
	emit polygonChanged(adjustPolygonToScreen(poly));
}

void MainWindow::updateLabelColor()
{
	auto button = ui->fillColorButton;
	button->setAutoFillBackground(true);
	auto palette = button->palette();
	palette.setColor(button->backgroundRole(), m_fill_color);
	button->setPalette(palette);
}

void MainWindow::setFillColor()
{
	m_fill_color = QColorDialog::getColor(
		Qt::black,
		this,
		"Pick a color",
		QColorDialog::DontUseNativeDialog
	);
	updateLabelColor();
	emit fillColorChanged(m_fill_color);
}
