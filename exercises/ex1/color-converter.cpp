#include <cstddef>
#include <cstdlib>
#include <optional>
#include <stdexcept>
#include <type_traits>
#include <variant>

#include "range/v3/all.hpp"
#include "cg/arithmetic-container.hpp"
#include "cg/arithmetic-container.hpp"
#include "fmt/format.h"
#include "fmt/ostream.h"
#include "cg/colors.hpp"

namespace rgs = ranges;
namespace vws = rgs::views;

constexpr auto error_msg =
R"__(ERROR: {2}

______________________________________
{0} is a color conversion tool

USAGE: {0} <from> <to> <v0> <v1> <v2>

<from> and <to> are color models

the supported color models are = ({1})

)__";

auto get_color_model_names()
{

	std::stringstream ss;

	auto last = end(cg::colors::model_names) - 1;

	std::for_each(
		begin(cg::colors::model_names),
		last,
		[&] (auto&& model)
		{
			ss << model << ", ";
		}
	);

	ss << *last;

	return ss.str();
}

using color_type = std::variant<
	cg::colors::rgb,
	cg::colors::xyz,
	cg::colors::cmy,
	cg::colors::hsv,
	cg::colors::cielab
>;

template<class To, class From>
auto color_converter_to(From color_a)
{
	To color_b = cg::colors::convert_to<To>(color_a);

	if constexpr(std::is_same_v<To, cg::colors::rgb>){
		cg::arithmetic_container<int32_t, 3> printable;

		rgs::copy(
			color_b | vws::transform(util::make_cast<int32_t>()),
			std::begin(printable)
		);

		fmt::print("{} -> {} {}\n",
			cg::colors::name_of<From>,
			cg::colors::name_of<To>,
			printable
		);
	}else{
		fmt::print("{} -> {} {}\n",
			cg::colors::name_of<From>,
			cg::colors::name_of<To>,
			color_b
		);
	}
}

auto color_from_string(std::string_view str) -> color_type
{
	using namespace cg::colors;

	switch(to_model(str)){
	case model::RGB:
		return rgb{};
		break;
	case model::HSV:
		return hsv{};
		break;
	case model::XYZ:
		return xyz{};
		break;
	case model::CMY:
		return cmy{};
		break;
	case model::CIELAB:
		return cielab{};
		break;
	default:
		break;
	}

	throw std::runtime_error(fmt::format("unknown type {}", str));
}

int main(int const argc, char const* const* const argv)
try{

	if(argc != 6 && argc != 5)
		throw std::runtime_error("wrong number of arguments!");

	auto make_loader = [](auto v0, auto v1, auto v2)
	{
		return [=](auto&& c)
		{
			c[0] = std::strtod(v0, nullptr);
			c[1] = std::strtod(v1, nullptr);
			c[2] = std::strtod(v2, nullptr);
		};
	};

	auto converter = [](auto&& a, auto&& b)
	{
		using T = std::decay_t<decltype(b)>;
		color_converter_to<T>(a);
	};

	if(argc == 6){
		auto a_color = color_from_string(argv[1]);
		auto b_color = color_from_string(argv[2]);
		std::visit(make_loader(argv[3], argv[4], argv[5]), a_color);
		std::visit(converter, a_color, b_color);
	}else{
		using namespace cg::colors;

		auto a_color = color_from_string(argv[1]);
		std::visit(make_loader(argv[2], argv[3], argv[4]), a_color);

		std::array color_types{
			color_type{ rgb{} },
			color_type{ xyz{} },
			color_type{ hsv{} },
			color_type{ cmy{} },
			color_type{ cielab{} }
		};

		for(auto&& type : color_types)
			std::visit(converter, a_color, type);
	}

	return EXIT_SUCCESS;
}catch(std::runtime_error& e){

	fmt::print(
		std::cerr,
		error_msg,
		argv[0],
		get_color_model_names(),
		e.what()
	);

	return EXIT_FAILURE;
}
